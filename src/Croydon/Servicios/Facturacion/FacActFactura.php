<?php

namespace Croydon\Servicios\Facturacion;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of FacActFactura
 *
 * @author jonathan
 */
class FacActFactura extends AbstractRequest {

    /**
     * @var string
     */
    protected $cadenaActFactura;

    /**
     * @return string
     */
    public function getCadenaActFactura() {
        return $this->cadenaActFactura;
    }

    /**
     * @param string $cadenaActFactura
     */
    public function setCadenaActFactura($cadenaActFactura) {
        $this->cadenaActFactura = $cadenaActFactura;
    }

    /**
     * @param string $nit
     * @param string $compania
     * @param string $ano
     * @param string $modulo
     * @param string $cedula
     * @param string $tipo_documento
     * @param string $nombres_factura
     * @param string $apellidos_factura
     * @param string $email
     * @param string $direccion_factura
     * @param string $direccion_despacho
     * @param string $nombres_despacho
     * @param string $apellidos_despacho
     * @param string $municipio_factura
     * @param string $municipio_despacho
     * @param string $pais
     * @param string $telefono_factura
     * @param string $celular_factura
     * @param string $telefono_despacho
     * @param string $celular_despacho
     * @param string $codigo_clase
     * @param string $codigo_condicion_pago
     * @param string $codigo_descuento
     * @param string $fecha_documento
     * @param string $orden_compra
     * @param string $subtotal_bruto
     * @param string $subtotal_fletes
     * @param string $total_cargos
     * @param string $iva
     * @param string $total_descuentos
     * @param string $total_neto
     * @param string $numDoc
     * @param array $detalle
     */
    public function __prepare(string $nit, string $compania, string $ano, string $modulo, string $cedula, string $tipo_documento, string $nombres_factura, string $apellidos_factura, string $email, string $direccion_factura, string $direccion_despacho, string $nombres_despacho, string $apellidos_despacho, string $municipio_factura, string $municipio_despacho, string $pais, string $telefono_factura, string $celular_factura, string $telefono_despacho, string $celular_despacho, string $codigo_clase, string $codigo_condicion_pago, string $codigo_descuento, string $fecha_documento, string $orden_compra, string $subtotal_bruto, string $subtotal_fletes, string $total_cargos, string $iva, string $total_descuentos, string $total_neto, string $numDoc, array $detalle) {
        $values = $this->__getValues(__FUNCTION__, func_get_args());
        unset($values['nit']);
        unset($values['compania']);
        unset($values['ano']);
        unset($values['modulo']);
        unset($values['detalle']);
        $arrayString = array();
        foreach ($detalle as $item) {
            $arrayString[] = sprintf('"%s"', implode('";"', $item));
        }
        $detailLine = implode('|', $arrayString);
        $generalData = sprintf('"%s"', implode('";"', $values));
        $this->cadenaActFactura = sprintf('"%s"|"%s"|"%s"|"%s"|%s|%s', $nit, $compania, $ano, $modulo, $generalData, $detailLine);
    }

}
