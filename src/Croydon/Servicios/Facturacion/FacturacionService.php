<?php

namespace Croydon\Servicios\Facturacion;

use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Croydon\Servicios\Service;

/**
 * Description of FacturacionService
 *
 * @author jonathan
 */
class FacturacionService extends Service {


    public function __construct(string $WSDL)
    {
        parent::__construct($WSDL);
    }

    /**
     * Inicialize $this->interface and $this->objectFactory
     */
    public function __init() {
        $this->interface = InterfaceFacturacion::class;
        $this->objectFactory = new ObjectFactory();
    }

    /**
     * @return InterfaceFacturacion
     * @throws MethodNotFoundException
     * @throws InvokeServiceException
     */
    public function getFacturacionPort() {
        try {
            return $this->getPort();
        } catch (MethodNotFoundException $ex) {
            throw new MethodNotFoundException($ex->getMessage());
        } catch (InvokeServiceException $ex) {
            throw new InvokeServiceException($ex);
        }
    }

}
