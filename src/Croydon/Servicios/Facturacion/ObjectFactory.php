<?php

namespace Croydon\Servicios\Facturacion;

use Croydon\Servicios\BaseObjectFactory;

/**
 * Description of ObjectFactory
 *
 * @author jonathan
 */
class ObjectFactory extends BaseObjectFactory {

    /**
     * @return FacActFactura
     */
    public function getFacActFactura(): FacActFactura {
        return new FacActFactura();
    }

    /**
     * @return FacActOrdenPedido
     */
    public function getFacActOrdenPedido(): FacActOrdenPedido {
        return new FacActOrdenPedido();
    }

    /**
     * @return FacActRedencionIncentivos
     */
    public function getFacActRedencionIncentivos(): FacActRedencionIncentivos {
        return new FacActRedencionIncentivos();
    }

}
