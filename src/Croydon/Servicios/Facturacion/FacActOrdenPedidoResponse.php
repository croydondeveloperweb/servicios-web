<?php

namespace Croydon\Servicios\Facturacion;

/**
 * Description of FacActOrdenPedidoResponse
 *
 * @author joncasasq
 */
class FacActOrdenPedidoResponse {

    /**
     * @var string
     */
    private $coddoc;

    /**
     * @var string
     */
    private $Sucdoc;

    /**
     * @var string
     */
    private $numdoc;

    /**
     * @var String
     */
    private $msg;

    public function getCoddoc() {
        return $this->coddoc;
    }

    public function getSucdoc() {
        return $this->Sucdoc;
    }

    public function getNumdoc() {
        return $this->numdoc;
    }

    public function getMsg(): String {
        return $this->msg;
    }

    public function setCoddoc($coddoc) {
        $this->coddoc = $coddoc;
    }

    public function setSucdoc($Sucdoc) {
        $this->Sucdoc = $Sucdoc;
    }

    public function setNumdoc($numdoc) {
        $this->numdoc = $numdoc;
    }

    public function setMsg(String $msg) {
        $this->msg = $msg;
    }

}
