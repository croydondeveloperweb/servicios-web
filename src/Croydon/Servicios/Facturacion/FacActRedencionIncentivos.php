<?php

namespace Croydon\Servicios\Facturacion;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of FacActRedencionIncentivos
 *
 * @author jonathan
 */
class FacActRedencionIncentivos extends AbstractRequest {

    /**
     * @var string
     */
    protected $cadenaIncentivos;

    /**
     * @return string
     */
    public function getCadenaIncentivos() {
        return $this->cadenaIncentivos;
    }

    /**
     * @param string $cadenaIncentivos
     */
    public function setCadenaIncentivos($cadenaIncentivos) {
        $this->cadenaIncentivos = $cadenaIncentivos;
    }

    /**
     * @param string $nit
     * @param string $compania
     * @param string $ano
     * @param string $modulo
     * @param string $cedula
     * @param array $items
     */
    public function __prepare(string $nit, string $compania, string $ano, string $modulo, string $cedula, array $items) {
        $values = $this->__getValues(__FUNCTION__, func_get_args());
        unset($values['items']);
        $arrayString = array();
        foreach ($items as $item) {
            $arrayString[] = sprintf('"%s"', implode('";"', $item));
        }
        unset($values['items']);
        $this->cadenaIncentivos = sprintf('"%s"|%s|##', implode('"|"', $values), implode('";"', $arrayString));
    }

}
