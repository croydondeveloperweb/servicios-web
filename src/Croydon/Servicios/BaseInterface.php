<?php

namespace Croydon\Servicios;

/**
 *
 * @author jonathan
 */
interface BaseInterface
{
    /**
     * @return mixed
     */
    public function __getLastRequest();

    /**
     * @return mixed
     */
    public function __getLastResponse();
}
