<?php

namespace Croydon\Servicios;

/**
 * Description of BaseService
 *
 * @author jonathan
 */
abstract class Service {

    /**
     * @var string the interface on which to work
     */
    protected $interface;

    /**
     * @var ConsumeService
     */
    private $consumeService;

    /**
     * @var BaseObjectFactory
     */
    protected $objectFactory;

    /**
     * @var string
     */
    protected $classToUse;

    /**
     * Base For invoke service
     * @param string $WSDL
     * @throws InvokeServiceException
     */
    public function __construct(string $WSDL) {
        try {
            $this->__init();
            $soapClient = new \SoapClient($WSDL, array('trace' => true));
            $this->consumeService = new ConsumeService($this->interface, $soapClient, $this->objectFactory);
        } catch (\Exception $ex) {
            throw new InvokeServiceException($ex);
        }
    }

    /**
     * Inicialize $this->interface and $this->objectFactory
     */
    public abstract function __init();

    /**
     * @return BaseInterface
     * @throws MethodNotFoundException
     * @throws InvokeServiceException
     */
    protected function getPort() {
        try {
            return $this->consumeService;
        } catch (MethodNotFoundException $ex) {
            throw new MethodNotFoundException($ex->getMessage());
        } catch (InvokeServiceException $ex) {
            throw new InvokeServiceException($ex);
        }
    }

}
