<?php

namespace Croydon\Servicios\Aurora;

use Croydon\Servicios\BaseObjectFactory;

/**
 * Description of ObjectFactory
 *
 * @author jonathan
 */
class AuroraObjectFactory extends BaseObjectFactory {

    /**
     * @return InvConsultaDispCroydonista
     */
    public function getInvConsultaDispCroydonista(): InvConsultaDispCroydonista {
        return new InvConsultaDispCroydonista();
    }

    /**
     * @return FacConsultaEstadoCartera
     */
    public function getFacConsultaEstadoCartera(): FacConsultaEstadoCartera {
        return new FacConsultaEstadoCartera();
    }

    /**
     * @return CarAplicacionFacturas
     */
    public function getCarAplicacionFacturas(): CarAplicacionFacturas {
        return new CarAplicacionFacturas();
    }

    /**
     * @return InvConsultaDispItemBod
     */
    public function getInvConsultaDispItemBod(): InvConsultaDispItemBod {
        return new InvConsultaDispItemBod();
    }

    /**
     * @return InvConsultaItemIncentivo
     */
    public function getInvConsultaItemIncentivo(): InvConsultaItemIncentivo {
        return new InvConsultaItemIncentivo();
    }

    /**
     * @return FacConsultaGuia
     */
    public function getFacConsultaGuia(): FacConsultaGuia {
        return new FacConsultaGuia();
    }

    /**
     * @return InvConsultaItemTiendas
     */
    public function getInvConsultaItemTiendas(): InvConsultaItemTiendas {
        return new InvConsultaItemTiendas();
    }

}
