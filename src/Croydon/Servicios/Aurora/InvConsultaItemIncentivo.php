<?php

namespace Croydon\Servicios\Aurora;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of InvConsultaItemIncentivo
 *
 * @author jonathan
 */
class InvConsultaItemIncentivo extends AbstractRequest{

    /**
     * @var string
     */
    protected $uid;

    /**
     * @var string
     */
    protected $cia;

    /**
     * @var string
     */
    protected $ano;

    /**
     * @var string
     */
    protected $app;

    /**
     * @var string
     */
    protected $codItem;

    /**
     * @var string
     */
    protected $cantidad;

    /**
     * @return string
     */
    public function getUid() {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid) {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getCia() {
        return $this->cia;
    }

    /**
     * @param string $cia
     */
    public function setCia(string $cia) {
        $this->cia = $cia;
    }

    /**
     * @return string
     */
    public function getAno() {
        return $this->ano;
    }

    /**
     * @param string $ano
     */
    public function setAno(string $ano) {
        $this->ano = $ano;
    }

    /**
     * @return string
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @param string $app
     */
    public function setApp(string $app) {
        $this->app = $app;
    }

    /**
     * @return string
     */
    public function getCodItem() {
        return $this->codItem;
    }

    /**
     * @param string $codItem
     */
    public function setCodItem(string $codItem) {
        $this->codItem = $codItem;
    }

    /**
     * @return string
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * @param string $cantidad
     */
    public function setCantidad(string $cantidad) {
        $this->cantidad = $cantidad;
    }

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $codItem
     * @param string $cantidad
     */
    public function __prepare(string $uid, string $cia, string $ano, string $app, string $codItem, string $cantidad) {
        $this->uid = $uid;
        $this->cia = $cia;
        $this->ano = $ano;
        $this->app = $app;
        $this->codItem = $codItem;
        $this->cantidad = $cantidad;
    }

}
