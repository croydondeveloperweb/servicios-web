<?php

namespace Croydon\Servicios\Aurora;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of FacConsultaGuia
 *
 * @author jonathan
 */
class FacConsultaGuia extends AbstractRequest {

    /**
     * @var string
     */
    protected $uid;

    /**
     * @var string
     */
    protected $cia;

    /**
     * @var string
     */
    protected $ano;

    /**
     * @var string
     */
    protected $app;

    /**
     * @var string
     */
    protected $orden;

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $orden
     */
    public function __prepare(string $uid, string $cia, string $ano, string $app, string $orden) {
        $this->uid = $uid;
        $this->cia = $cia;
        $this->ano = $ano;
        $this->app = $app;
        $this->orden = $orden;
    }

    /**
     * @return string
     */
    public function getUid() {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getCia() {
        return $this->cia;
    }

    /**
     * @return string
     */
    public function getAno() {
        return $this->ano;
    }

    /**
     * @return string
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @return string
     */
    public function getOrden() {
        return $this->orden;
    }

    /**
     * @return string
     */
    public function setUid(string $uid) {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function setCia(string $cia) {
        $this->cia = $cia;
    }

    /**
     * @return string
     */
    public function setAno(string $ano) {
        $this->ano = $ano;
    }

    /**
     * @return string
     */
    public function setApp(string $app) {
        $this->app = $app;
    }

    /**
     * @return string
     */
    public function setOrden(string $orden) {
        $this->orden = $orden;
    }

}
