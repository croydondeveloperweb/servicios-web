<?php

namespace Croydon\Servicios\Aurora;

/**
 * Description of FacConsultaEstadoCarteraResponse
 *
 * @author jonathan
 */
class FacConsultaEstadoCarteraResponse {

    /**
     * @var float
     */
    private $cupodisponible;

    /**
     * @var string
     */
    private $factura;

    /**
     * @var string
     */
    private $vlrnotacre;

    /**
     * @var float
     */
    private $saldo;

    /**
     * @var float
     */
    private $porredimir;

    /**
     * @var float
     */
    public function getCupodisponible() {
        return $this->cupodisponible;
    }

    /**
     * @var string
     */
    public function getFactura() {
        return $this->factura;
    }

    /**
     * @var string
     */
    public function getVlrnotacre() {
        return $this->vlrnotacre;
    }

    /**
     * @var float
     */
    public function getSaldo() {
        return $this->saldo;
    }

    /**
     * @var float
     */
    public function getPorredimir() {
        return $this->porredimir;
    }

    /**
     * @param float $cupodisponible
     */
    public function setCupodisponible(float $cupodisponible) {
        $this->cupodisponible = $cupodisponible;
    }

    /**
     * @param string $factura
     */
    public function setFactura(string $factura) {
        $this->factura = $factura;
    }

    /**
     * @param string $vlrnotacre
     */
    public function setVlrnotacre(string $vlrnotacre) {
        $this->vlrnotacre = $vlrnotacre;
    }

    /**
     * @param float $saldo
     */
    public function setSaldo(float $saldo) {
        $this->saldo = $saldo;
    }

    /**
     * @param float $porredimir
     */
    public function setPorredimir(float $porredimir) {
        $this->porredimir = $porredimir;
    }

}
