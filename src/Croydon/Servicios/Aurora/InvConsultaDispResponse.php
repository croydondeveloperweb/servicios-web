<?php

namespace Croydon\Servicios\Aurora;

/**
 * Description of InvConsultaDispItemBodResponse
 *
 * @author jonathan
 */
class InvConsultaDispResponse {

    /**
     * @var float
     */
    protected $CantDisponible;

    /**
     * @return float
     */
    public function getCantDisponible() {
        return $this->CantDisponible;
    }

    /**
     * @param float $CantDisponible
     */
    public function setCantDisponible($CantDisponible) {
        $this->CantDisponible = $CantDisponible;
    }

}
