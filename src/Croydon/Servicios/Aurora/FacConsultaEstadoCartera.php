<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Servicios\Aurora;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of FacConsultaEstadoCartera
 *
 * @author jonathan
 */
class FacConsultaEstadoCartera extends AbstractRequest {

    /**
     * @var string
     */
    protected $uid;

    /**
     * @var string
     */
    protected $cia;

    /**
     * @var string
     */
    protected $ano;

    /**
     * @var string
     */
    protected $app;

    /**
     * @var string
     */
    protected $cliente;

    public function __construct() {
        
    }

    /**
     * @return string
     */
    public function getUid() {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getCia() {
        return $this->cia;
    }

    /**
     * @return string
     */
    public function getAno() {
        return $this->ano;
    }

    /**
     * @return string
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @return string
     */
    public function getCliente() {
        return $this->cliente;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid) {
        $this->uid = $uid;
    }

    /**
     * @param string $cia
     */
    public function setCia(string $cia) {
        $this->cia = $cia;
    }

    /**
     * @param string $ano
     */
    public function setAno(string $ano) {
        $this->ano = $ano;
    }

    /**
     * @param string $app
     */
    public function setApp(string $app) {
        $this->app = $app;
    }

    /**
     * @param string $cliente
     */
    public function setCliente(string $cliente) {
        $this->cliente = $cliente;
    }

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $cliente
     */
    public function __prepare(string $uid, string $cia, string $ano, string $app, string $cliente) {
        $this->setAno($ano);
        $this->setApp($app);
        $this->setCia($cia);
        $this->setCliente($cliente);
        $this->setUid($uid);
    }

}
