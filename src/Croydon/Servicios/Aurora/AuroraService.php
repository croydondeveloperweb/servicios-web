<?php

namespace Croydon\Servicios\Aurora;

use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Croydon\Servicios\Service;

/**
 * Description of AuroraService
 *
 * @author jonathan
 */
class AuroraService extends Service
{
    /**
     * Base For invoke service
     * @param string $WSDL
     * @throws InvokeServiceException
     */
    public function __construct(string $WSDL)
    {
        parent::__construct($WSDL);
    }

    /**
     * Inicialize $this->interface and $this->objectFactory
     */
    public function __init()
    {
        $this->interface = InterfaceAurora::class;
        $this->objectFactory = new AuroraObjectFactory();
    }

    /**
     * @return InterfaceAurora
     * @throws MethodNotFoundException
     * @throws InvokeServiceException
     */
    public function getFacturacionPort()
    {
        try {
            return $this->getPort();
        } catch (MethodNotFoundException $ex) {
            throw new MethodNotFoundException($ex->getMessage());
        } catch (InvokeServiceException $ex) {
            throw new InvokeServiceException($ex);
        }
    }

}
