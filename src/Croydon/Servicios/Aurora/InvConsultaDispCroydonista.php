<?php

namespace Croydon\Servicios\Aurora;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of InvConsultaDispCroydonista
 *
 * @author jonathan
 */
class InvConsultaDispCroydonista extends AbstractRequest {

    /**
     * @var string
     */
    protected $uid;

    /**
     * @var string
     */
    protected $cia;

    /**
     * @var string
     */
    protected $ano;

    /**
     * @var string
     */
    protected $app;

    /**
     * @var string
     */
    protected $codItem;

    /**
     * @var string
     */
    protected $cantidad;

    /**
     * @return string
     */
    public function getUid() {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getCia() {
        return $this->cia;
    }

    /**
     * @return string
     */
    public function getAno() {
        return $this->ano;
    }

    /**
     * @return string
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @return string
     */
    public function getCodItem() {
        return $this->codItem;
    }

    /**
     * @return string
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid) {
        $this->uid = $uid;
    }

    /**
     * @param string $cia
     */
    public function setCia(string $cia) {
        $this->cia = $cia;
    }

    /**
     * @param string $ano
     */
    public function setAno(string $ano) {
        $this->ano = $ano;
    }

    /**
     * @param string $app
     */
    public function setApp(string $app) {
        $this->app = $app;
    }

    /**
     * @param string $codItem
     */
    public function setCodItem(string $codItem) {
        $this->codItem = $codItem;
    }

    /**
     * @param string $cantidad
     */
    public function setCantidad(string $cantidad) {
        $this->cantidad = $cantidad;
    }

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $codItem
     * @param string $cantidad
     */
    public function __prepare(string $uid, string $cia, string $ano, string $app, string $codItem, string $cantidad) {
        $this->setAno($ano);
        $this->setApp($app);
        $this->setCantidad($cantidad);
        $this->setCia($cia);
        $this->setCodItem($codItem);
        $this->setUid($uid);
    }

}
