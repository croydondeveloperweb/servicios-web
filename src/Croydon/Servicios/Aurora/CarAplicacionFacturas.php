<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Servicios\Aurora;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of CarAplicacionFacturas
 *
 * @author jonathan
 */
class CarAplicacionFacturas extends AbstractRequest {

    /**
     * @var string
     */
    protected $uid;

    /**
     * @var string
     */
    protected $cia;

    /**
     * @var string
     */
    protected $ano;

    /**
     * @var string
     */
    protected $app;

    /**
     * @var string
     */
    protected $cliente;

    /**
     * @var string
     */
    protected $sucursal;

    /**
     * @var string
     */
    protected $factura;

    /**
     * @var string
     */
    protected $parcial;

    /**
     * @return string
     */
    public function getUid() {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getCia() {
        return $this->cia;
    }

    /**
     * @return string
     */
    public function getAno() {
        return $this->ano;
    }

    /**
     * @return string
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @return string
     */
    public function getCliente() {
        return $this->cliente;
    }

    /**
     * @return string
     */
    public function getSucursal() {
        return $this->sucursal;
    }

    /**
     * @return string
     */
    public function getFactura() {
        return $this->factura;
    }

    /**
     * @return string
     */
    public function getParcial() {
        return $this->parcial;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid) {
        $this->uid = $uid;
    }

    /**
     * @param string $cia
     */
    public function setCia($cia) {
        $this->cia = $cia;
    }

    /**
     * @param string $ano
     */
    public function setAno($ano) {
        $this->ano = $ano;
    }

    /**
     * @param string $app
     */
    public function setApp($app) {
        $this->app = $app;
    }

    /**
     * @param string $cliente
     */
    public function setCliente($cliente) {
        $this->cliente = $cliente;
    }

    /**
     * @param string $sucursal
     */
    public function setSucursal($sucursal) {
        $this->sucursal = $sucursal;
    }

    /**
     * @param string $factura
     */
    public function setFactura($factura) {
        $this->factura = $factura;
    }

    /**
     * @param string $parcial
     */
    public function setParcial($parcial) {
        $this->parcial = $parcial;
    }

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $cliente
     * @param string $sucursal
     * @param string $factura
     * @param string $parcial
     */
    public function __prepare($uid, $cia, $ano, $app, $cliente, $sucursal, $factura, $parcial) {
        $this->uid = $uid;
        $this->cia = $cia;
        $this->ano = $ano;
        $this->app = $app;
        $this->cliente = $cliente;
        $this->sucursal = $sucursal;
        $this->factura = $factura;
        $this->parcial = $parcial;
    }

}
