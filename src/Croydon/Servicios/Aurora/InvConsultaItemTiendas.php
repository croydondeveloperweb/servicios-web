<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Servicios\Aurora;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of InvConsultaItemTiendas
 *
 * @author jonathan
 */
class InvConsultaItemTiendas extends AbstractRequest {

    /**
     * @var string
     */
    protected $cia;

    /**
     * @var string
     */
    protected $ano;

    /**
     * @var string
     */
    protected $app;

    /**
     * @var string
     */
    protected $codItem;

    /**
     * @var string
     */
    protected $codCiudad;

    /**
     * @return string
     */
    public function getCia() {
        return $this->cia;
    }

    /**
     * @return string
     */
    public function getAno() {
        return $this->ano;
    }

    /**
     * @return string
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @return string
     */
    public function getCodItem() {
        return $this->codItem;
    }

    /**
     * @return string
     */
    public function getCodCiudad() {
        return $this->codCiudad;
    }

    /**
     * @param string $cia
     */
    public function setCia(string $cia) {
        $this->cia = $cia;
    }

    /**
     * @param string $ano
     */
    public function setAno(string $ano) {
        $this->ano = $ano;
    }

    /**
     * @param string $app
     */
    public function setApp(string $app) {
        $this->app = $app;
    }

    /**
     * @param string $codItem
     */
    public function setCodItem(string $codItem) {
        $this->codItem = $codItem;
    }

    /**
     * @param string $codCiudad
     */
    public function setCodCiudad(string $codCiudad) {
        $this->codCiudad = $codCiudad;
    }

    /**
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $codItem
     * @param string $codCiudad
     */
    public function __prepare(string $cia, string $ano, string $app, string $codItem, string $codCiudad) {
        $this->cia = $cia;
        $this->ano = $ano;
        $this->app = $app;
        $this->codItem = $codItem;
        $this->codCiudad = $codCiudad;
    }

}
