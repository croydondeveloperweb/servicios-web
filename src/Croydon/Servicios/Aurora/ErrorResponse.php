<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Servicios\Aurora;

/**
 * Description of ErrorResponse
 *
 * @author jonathan
 */
class ErrorResponse {

    /**
     * @var string
     */
    protected $codigo;

    /**
     * @var string
     */
    protected $descripcion;

    /**
     * @return string
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * @param string $codigo
     */
    public function setCodigo(string $codigo) {
        $this->codigo = $codigo;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion(string $descripcion) {
        $this->descripcion = $descripcion;
    }

}
