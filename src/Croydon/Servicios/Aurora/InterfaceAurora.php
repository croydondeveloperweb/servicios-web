<?php

namespace Croydon\Servicios\Aurora;

use Croydon\Servicios\BaseInterface;

/**
 *
 * @author jonathan
 */
interface InterfaceAurora extends BaseInterface {

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $cliente
     * @param string $sucursal
     * @param string $factura
     * @param string $parcial
     * @return string
     */
    public function carAplicacionFacturas(string $uid, string $cia, string $ano, string $app, string $cliente, string $sucursal, string $factura, string $parcial);

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $codItem
     * @param string $cantidad
     * @return InvConsultaDispResponse|ErrorResponse
     */
    public function invConsultaDispCroydonista(string $uid, string $cia, string $ano, string $app, string $codItem, string $cantidad);

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $codItem
     * @param string $cantidad
     * @return InvConsultaDispResponse|ErrorResponse
     */
    public function invConsultaDispItemBod(string $uid, string $cia, string $ano, string $app, string $codItem, string $cantidad);

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $codItem
     * @param string $cantidad
     * @return InvConsultaDispResponse|ErrorResponse
     */
    public function invConsultaItemIncentivo(string $uid, string $cia, string $ano, string $app, string $codItem, string $cantidad);

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $cliente
     * @return FacConsultaEstadoCarteraResponse|ErrorResponse
     */
    public function facConsultaEstadoCartera(string $uid, string $cia, string $ano, string $app, string $cliente);

    /**
     * @param string $uid
     * @param string $cia
     * @param string $ano
     * @param string $app
     * @param string $orden
     * @return string
     */
    public function facConsultaGuia(string $uid, string $cia, string $ano, string $app, string $orden);

    public function InvConsultaItemTiendas();
}
