<?php

namespace Croydon\Servicios\Pse;

use Croydon\Servicios\InvokeServiceException;
use Croydon\Servicios\MethodNotFoundException;
use Croydon\Servicios\Service;

/**
 * Description of PseService
 *
 * @author jonathan
 */
class PseService extends Service
{

    public function __construct(string $WSDL)
    {
        parent::__construct($WSDL);
    }

    /**
     * Inicialize $this->interface and $this->objectFactory
     */
    public function __init()
    {
        $this->interface = InterfacePse::class;
        $this->objectFactory = new ObjectFactory();
        //$this->WSDL = 'https://test1.e-collect.com/d_Express/webservice/eCollectWebservicesv2.asmx?wsdl';
    }

    /**
     * @return InterfacePse
     * @throws MethodNotFoundException
     * @throws InvokeServiceException
     */
    public function getPsePort()
    {
        try {
            return $this->getPort();
        } catch (MethodNotFoundException $ex) {
            throw new MethodNotFoundException($ex->getMessage());
        } catch (InvokeServiceException $ex) {
            throw new InvokeServiceException($ex);
        }
    }

}
