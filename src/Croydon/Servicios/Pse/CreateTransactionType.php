<?php

namespace Croydon\Servicios\Pse;

/**
 * Description of CreateTransactionType
 *
 * @author jonathan
 */
class CreateTransactionType {

    /**
     * @var string
     */
    protected $entityCode;

    /**
     * @var string
     */
    protected $srvCode;

    /**
     * @var string
     */
    protected $transValue;

    /**
     * @var string
     */
    protected $transVatValue;

    /**
     * @var string
     */
    protected $srvCurrency;

    /**
     * @var string
     */
    protected $urlResponse;

    /**
     * @var string
     */
    protected $urlRedirect;

    /**
     * @var string
     */
    protected $sign;

    /**
     * @var string
     */
    protected $signFields;

    /**
     * @var array
     */
    protected $referenceArray;

    /**
     * @param string $entityCode
     * @param string $srvCode
     * @param string $transValue
     * @param string $transVatValue
     * @param string $srvCurrency
     * @param string $urlResponse
     * @param string $urlRedirect
     * @param string $signFields
     * @param array $referenceArray
     */
    public function __prepare(string $entityCode, string $srvCode, string $transValue, string $transVatValue, string $srvCurrency, string $urlResponse, string $urlRedirect, string $sign, string $signFields, array $referenceArray) {
        $this->entityCode = $entityCode;
        $this->srvCode = $srvCode;
        $this->transValue = $transValue;
        $this->transVatValue = $transVatValue;
        $this->srvCurrency = $srvCurrency;
        $this->urlResponse = $urlResponse;
        $this->urlRedirect = $urlRedirect;
        $this->sign = $sign;
        $this->signFields = $signFields;
        $this->referenceArray = $referenceArray;
    }

    /**
     * @return string
     */
    public function getEntityCode() {
        return $this->entityCode;
    }

    /**
     * @return string
     */
    public function getSrvCode() {
        return $this->srvCode;
    }

    /**
     * @return string
     */
    public function getTransValue() {
        return $this->transValue;
    }

    /**
     * @return string
     */
    public function getTransVatValue() {
        return $this->transVatValue;
    }

    /**
     * @return string
     */
    public function getSrvCurrency() {
        return $this->srvCurrency;
    }

    /**
     * @return string
     */
    public function getUrlResponse() {
        return $this->urlResponse;
    }

    /**
     * @return string
     */
    public function getUrlRedirect() {
        return $this->urlRedirect;
    }

    /**
     * @return string
     */
    public function getSign() {
        return $this->sign;
    }

    /**
     * @return string
     */
    public function getSignFields() {
        return $this->signFields;
    }

    /**
     * @return array
     */
    public function getReferenceArray() {
        return $this->referenceArray;
    }

    /**
     * @param string $entityCode
     */
    public function setEntityCode(string $entityCode) {
        $this->entityCode = $entityCode;
    }

    /**
     * @param string $srvCode
     */
    public function setSrvCode(string $srvCode) {
        $this->srvCode = $srvCode;
    }

    /**
     * @param string $transValue
     */
    public function setTransValue(string $transValue) {
        $this->transValue = $transValue;
    }

    /**
     * @param string $transVatValue
     */
    public function setTransVatValue(string $transVatValue) {
        $this->transVatValue = $transVatValue;
    }

    /**
     * @param string $srvCurrency
     */
    public function setSrvCurrency(string $srvCurrency) {
        $this->srvCurrency = $srvCurrency;
    }

    /**
     * @param string $urlResponse
     */
    public function setUrlResponse(string $urlResponse) {
        $this->urlResponse = $urlResponse;
    }

    /**
     * @param string $urlRedirect
     */
    public function setUrlRedirect(string $urlRedirect) {
        $this->urlRedirect = $urlRedirect;
    }

    /**
     * @param string $sign
     */
    public function setSign(string $sign) {
        $this->sign = $sign;
    }

    /**
     * @param string $signFields
     */
    public function setSignFields(string $signFields) {
        $this->signFields = $signFields;
    }

    /**
     * @param array $referenceArray
     */
    public function setReferenceArray(array $referenceArray) {
        $this->referenceArray = $referenceArray;
    }

}
