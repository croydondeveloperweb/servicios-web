<?php

/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 28/03/19
 * Time: 8:46
 */

namespace Croydon\Servicios\Pse;

class CreateTransactionResponseType
{

    /**
     * @var string
     */
    protected $returnCode;

    /**
     * @var string
     */
    protected $ticketId;

    /**
     * @var string
     */
    protected $eCollectUrl;

    /**
     * @return string
     */
    public function getReturnCode()
    {
        return $this->returnCode;
    }

    /**
     * @param string $returnCode
     */
    public function setReturnCode($returnCode)
    {
        $this->returnCode = $returnCode;
    }

    /**
     * @return string
     */
    public function getTicketId()
    {
        return $this->ticketId;
    }

    /**
     * @param string $ticketId
     */
    public function setTicketId($ticketId)
    {
        $this->ticketId = $ticketId;
    }

    /**
     * @return string
     */
    public function getECollectUrl()
    {
        return $this->eCollectUrl;
    }

    /**
     * @param string $eCollectUrl
     */
    public function setECollectUrl($eCollectUrl)
    {
        $this->eCollectUrl = $eCollectUrl;
    }

}
