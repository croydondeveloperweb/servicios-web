<?php

namespace Croydon\Servicios\Pse;

/**
 * Description of GetTransactionInformationType
 *
 * @author jonathan
 */
class GetTransactionInformationType {

    /**
     * @var string
     */
    protected $entityCode;

    /**
     * @var string
     */
    protected $ticketId;

    /**
     * @return string
     */
    public function getEntityCode() {
        return $this->entityCode;
    }

    /**
     * @return string
     */
    public function getTicketId() {
        return $this->ticketId;
    }

    /**
     * @param string $entityCode
     */
    public function setEntityCode(string $entityCode) {
        $this->entityCode = $entityCode;
    }

    /**
     * @param string $ticketId
     */
    public function setTicketId(string $ticketId) {
        $this->ticketId = $ticketId;
    }

    /**
     * @param string $entityCode
     * @param string $ticketId
     */
    public function __prepare(string $entityCode, string $ticketId) {
        $this->entityCode = $entityCode;
        $this->ticketId = $ticketId;
    }

}
