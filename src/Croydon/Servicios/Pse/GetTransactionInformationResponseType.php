<?php

namespace Croydon\Servicios\Pse;

/**
 * Description of GetTransactionInformationResponseType
 *
 * @author joncasasq
 */
class GetTransactionInformationResponseType {

    /**
     * @var string
     */
    protected $entityCode;

    /**
     * @var string
     */
    protected $ticketId;

    /**
     * @var string
     */
    protected $trazabilityCode;

    /**
     * @var string
     */
    protected $tranState;

    /**
     * @var string
     */
    protected $returnCode;

    /**
     * @var float
     */
    protected $transValue;

    /**
     * @var float
     */
    protected $transVatValue;

    /**
     * @var string
     */
    protected $payCurrency;

    /**
     * @var float
     */
    protected $currencyRate;

    /**
     * @var float
     */
    protected $bankProcessDate;

    /**
     * @var float
     */
    protected $bankName;

    /**
     * @var float
     */
    protected $paymentSystem;

    /**
     * @var array
     */
    protected $referenceArray;

    /**
     * @var array
     */
    protected $retriesTicketId;

    /**
     * @return string
     */
    public function getEntityCode()
    {
        return $this->entityCode;
    }

    /**
     * @param string $entityCode
     */
    public function setEntityCode($entityCode)
    {
        $this->entityCode = $entityCode;
    }

    /**
     * @return string
     */
    public function getTicketId()
    {
        return $this->ticketId;
    }

    /**
     * @param string $ticketId
     */
    public function setTicketId($ticketId)
    {
        $this->ticketId = $ticketId;
    }

    /**
     * @return string
     */
    public function getTrazabilityCode()
    {
        return $this->trazabilityCode;
    }

    /**
     * @param string $trazabilityCode
     */
    public function setTrazabilityCode($trazabilityCode)
    {
        $this->trazabilityCode = $trazabilityCode;
    }

    /**
     * @return string
     */
    public function getTranState()
    {
        return $this->tranState;
    }

    /**
     * @param string $tranState
     */
    public function setTranState($tranState)
    {
        $this->tranState = $tranState;
    }

    /**
     * @return string
     */
    public function getReturnCode()
    {
        return $this->returnCode;
    }

    /**
     * @param string $returnCode
     */
    public function setReturnCode($returnCode)
    {
        $this->returnCode = $returnCode;
    }

    /**
     * @return float
     */
    public function getTransValue()
    {
        return $this->transValue;
    }

    /**
     * @param float $transValue
     */
    public function setTransValue($transValue)
    {
        $this->transValue = $transValue;
    }

    /**
     * @return float
     */
    public function getTransVatValue()
    {
        return $this->transVatValue;
    }

    /**
     * @param float $transVatValue
     */
    public function setTransVatValue($transVatValue)
    {
        $this->transVatValue = $transVatValue;
    }

    /**
     * @return string
     */
    public function getPayCurrency()
    {
        return $this->payCurrency;
    }

    /**
     * @param string $payCurrency
     */
    public function setPayCurrency($payCurrency)
    {
        $this->payCurrency = $payCurrency;
    }

    /**
     * @return float
     */
    public function getCurrencyRate()
    {
        return $this->currencyRate;
    }

    /**
     * @param float $currencyRate
     */
    public function setCurrencyRate($currencyRate)
    {
        $this->currencyRate = $currencyRate;
    }

    /**
     * @return float
     */
    public function getBankProcessDate()
    {
        return $this->bankProcessDate;
    }

    /**
     * @param float $bankProcessDate
     */
    public function setBankProcessDate($bankProcessDate)
    {
        $this->bankProcessDate = $bankProcessDate;
    }

    /**
     * @return float
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param float $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return float
     */
    public function getPaymentSystem()
    {
        return $this->paymentSystem;
    }

    /**
     * @param float $paymentSystem
     */
    public function setPaymentSystem($paymentSystem)
    {
        $this->paymentSystem = $paymentSystem;
    }

    /**
     * @return array
     */
    public function getReferenceArray()
    {
        return $this->referenceArray;
    }

    /**
     * @param array $referenceArray
     */
    public function setReferenceArray($referenceArray)
    {
        $this->referenceArray = $referenceArray;
    }

    /**
     * @return array
     */
    public function getRetriesTicketId()
    {
        return $this->retriesTicketId;
    }

    /**
     * @param array $retriesTicketId
     */
    public function setRetriesTicketId($retriesTicketId)
    {
        $this->retriesTicketId = $retriesTicketId;
    }
    
    

}
