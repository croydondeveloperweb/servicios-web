<?php

namespace Croydon\Servicios\Pse;

use Croydon\Servicios\BaseObjectFactory;

/**
 * Description of ObjectFactory
 *
 * @author jonathan
 */
class ObjectFactory extends BaseObjectFactory {

    /**
     * @return GetTransactionInformation
     */
    public function getGetTransactionInformation(): GetTransactionInformation {
        return new GetTransactionInformation();
    }

    /**
     * @return CreateTransactionPayment
     */
    public function getCreateTransactionPayment(): CreateTransactionPayment {
        return new CreateTransactionPayment();
    }

}
