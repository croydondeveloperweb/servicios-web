<?php

namespace Croydon\Servicios\Pse;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of GetTransactionInformation
 *
 * @author jonathan
 */
class GetTransactionInformation extends AbstractRequest {

    /**
     * @var GetTransactionInformationType
     */
    protected $request;

    /**
     * @return GetTransactionInformationType
     */
    public function getRequest(): GetTransactionInformationType {
        return $this->request;
    }

    /**
     * @param GetTransactionInformationType $request
     */
    public function setRequest(GetTransactionInformationType $request) {
        $this->request = $request;
    }

    /**
     * @param string $entityCode
     * @param string $ticketId
     */
    public function __prepare(string $entityCode, string $ticketId) {
        $this->request = new GetTransactionInformationType();
        $this->request->__prepare($entityCode, $ticketId);
    }

}
