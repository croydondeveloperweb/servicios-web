<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Servicios\Pse;

/**
 * Description of GetTransactionInformationResponse
 *
 * @author joncasasq
 */
class GetTransactionInformationResponse
{

    /**
     * @var GetTransactionInformationResponseType
     */
    protected $getTransactionInformationResult;

    /**
     * @return GetTransactionInformationResponseType
     */
    public function getGetTransactionInformationResult()
    {
        return $this->getTransactionInformationResult;
    }

    /**
     * @param GetTransactionInformationResponseType $getTransactionInformationResult
     */
    public function setGetTransactionInformationResult($getTransactionInformationResult)
    {
        $this->getTransactionInformationResult = $getTransactionInformationResult;
    }


}
