<?php

namespace Croydon\Servicios\Pse;

use Croydon\Servicios\BaseInterface;

/**
 * Description of InterfacePse
 *
 * @author jonathan
 */
interface InterfacePse extends BaseInterface
{

    /**
     * @param string $entityCode
     * @param string $srvCode
     * @param string $transValue
     * @param string $transVatValue
     * @param string $srvCurrency
     * @param string $urlResponse
     * @param string $urlRedirect
     * @param string $signFields
     * @param array $referenceArray
     * @return CreateTransactionPaymentResponse
     */
    public function createTransactionPayment(string $entityCode, string $srvCode, string $transValue, string $transVatValue, string $srvCurrency, string $urlResponse, string $urlRedirect, string $sign, string $signFields, array $referenceArray);

    /**
     * @param string $entityCode
     * @param string $ticketId
     * @return GetTransactionInformationResponse
     */
    public function getTransactionInformation(string $entityCode, string $ticketId);
}
