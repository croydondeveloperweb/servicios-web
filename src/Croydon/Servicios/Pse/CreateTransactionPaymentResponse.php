<?php

/**
 * Created by PhpStorm.
 * User: joncasasq
 * Date: 28/03/19
 * Time: 8:45
 */

namespace Croydon\Servicios\Pse;

class CreateTransactionPaymentResponse {

    /**
     * @var CreateTransactionResponseType
     */
    protected $createTransactionPaymentResult;

    /**
     * @return CreateTransactionResponseType
     */
    public function getCreateTransactionPaymentResult(): CreateTransactionResponseType {
        return $this->createTransactionPaymentResult;
    }

    /**
     * @param CreateTransactionResponseType $createTransactionPaymentResult
     */
    public function setCreateTransactionPaymentResult(CreateTransactionResponseType $createTransactionPaymentResult) {
        $this->createTransactionPaymentResult = $createTransactionPaymentResult;
    }

}
