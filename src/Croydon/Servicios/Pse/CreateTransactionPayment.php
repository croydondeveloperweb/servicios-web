<?php

namespace Croydon\Servicios\Pse;

use Croydon\Servicios\AbstractRequest;

/**
 * Description of CreateTransactionPayment
 *
 * @author jonathan
 */
class CreateTransactionPayment extends AbstractRequest {

    /**
     * @var CreateTransactionType
     */
    protected $request;

    public function getRequest(): CreateTransactionType {
        return $this->request;
    }

    public function setRequest(CreateTransactionType $request) {
        $this->request = $request;
    }

    /**
     * @param string $entityCode
     * @param string $srvCode
     * @param string $transValue
     * @param string $transVatValue
     * @param string $srvCurrency
     * @param string $urlResponse
     * @param string $urlRedirect
     * @param string $signFields
     * @param array $referenceArray
     */
    public function __prepare(string $entityCode, string $srvCode, string $transValue, string $transVatValue, string $srvCurrency, string $urlResponse, string $urlRedirect, string $sign, string $signFields, array $referenceArray) {
        $this->request = new CreateTransactionType();
        $this->request->__prepare($entityCode, $srvCode, $transValue, $transVatValue, $srvCurrency, $urlResponse, $urlRedirect, $sign, $signFields, $referenceArray);
    }

}
