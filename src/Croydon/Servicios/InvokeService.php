<?php

namespace Croydon\Servicios;

/**
 * Description of InvokeService
 *
 * @author jonathan
 */
class InvokeService {

    /**
     * @var string
     */
    const method_prepare = '__prepare';

    /**
     * @var \SoapClient
     */
    private $soapClient;

    /**
     * @var BaseObjectFactory
     */
    private $objectFactory;

    /**
     * @var \ReflectionClass
     */
    private $reflection;

    /**
     * @var \ReflectionClass
     */
    private $reflectionFactory;

    /**
     * @var string
     */
    private $interface;

    /**
     * @var string
     */
    private $methodName;

    /**
     * @var AbstractRequest
     */
    private $abstractRequest;

    /**
     * @var \ReflectionClass
     */
    private $reflectionRequest;

    /**
     * @var string
     */
    private $soapParam;

    /**
     * @var string
     */
    private $baseName;

    /**
     * @param \SoapClient $soapClient
     * @param BaseObjectFactory $objectFactory
     * @param string $interface
     * @param string $baseName
     */
    public function __construct(\SoapClient $soapClient, BaseObjectFactory $objectFactory, string $interface, string $baseName) {
        $this->soapClient = $soapClient;
        $this->objectFactory = $objectFactory;
        $this->interface = $interface;
        $this->baseName = $baseName;
    }

    /**
     * Crea los los objetos de refleccion para acceder a las propiedades
     */
    public function __init() {
        $this->reflection = new \ReflectionClass($this->interface);
        $this->reflectionFactory = new \ReflectionClass($this->objectFactory);
    }

    /**
     * Valida que el metodo invocado de la interface exista
     */
    public function __validateMethod() {
        if (!$this->reflection->hasMethod($this->baseName)) {
            throw new MethodNotFoundException(sprintf('Method %s not found in class %s', $this->baseName, $this->reflection->getName()));
        }
    }

    /**
     * Valida que el metodo para fabricacion de objetos exista, eso se usa para las instancias de los objetos que tienen las propiedades que hacen la peticion
     */
    public function __validateMethodFactory() {
        $this->methodName = sprintf('get%s', ucfirst($this->baseName));
        if (!$this->reflectionFactory->hasMethod($this->methodName)) {
            throw new MethodNotFoundException(sprintf('Method %s not found in class %s', $this->methodName, $this->reflectionFactory->getName()));
        }
    }

    /**
     * Crea el objeto abstract request el cual hace la gestion de las propiedades que se deben enviar en el servicio
     */
    public function __createAbstractRequest() {
        $this->abstractRequest = $this->reflectionFactory->getMethod($this->methodName)->invoke($this->objectFactory);
    }

    /**
     * Valida que el metodo prepare exista en la instanacia creada en el metodo  __createAbstractRequest
     */
    public function __validateMethodPrepare() {
        $this->reflectionRequest = new \ReflectionClass($this->abstractRequest);
        if (!$this->reflectionRequest->hasMethod(self::method_prepare)) {
            throw new MethodNotFoundException(sprintf('Method %s not found in class %s', self::method_prepare, $this->reflectionRequest->getName()));
        }
    }

    /**
     * Crea las propiedades del servicio con la parametrizacion soap necesaria
     */
    public function __prepare($args) {
        $this->reflectionRequest->getMethod(self::method_prepare)->invokeArgs($this->abstractRequest, $args);
    }

    /**
     * Crea la variable que contienen las propiedades del servicio
     */
    public function __soapParam() {
        $this->soapParam = new \SoapParam($this->abstractRequest->toRequest(), ucfirst($this->baseName));
    }

    /**
     * Hace el llamado al servicio soap
     * @throws InvokeServiceException
     */
    public function __invokeService() {
        try {
            return $this->soapClient->__soapCall(ucfirst($this->baseName), array($this->soapParam));
        } catch (\Exception $ex) {
            throw new InvokeServiceException($ex);
        }
    }

    /**
     * @return mixed
     */
    public function __getLastRequest() {
        return $this->soapClient->__getLastRequest();
    }

    /**
     * @return mixed
     */
    public function __getLastResponse() {
        return $this->soapClient->__getLastResponse();
    }

}
