<?php

namespace Croydon\Servicios;

/**
 * Description of ConsumeService
 *
 * @author jonathan
 */
class ConsumeService {

    /**
     * @var string
     */
    private $interface;

    /**
     * @var \SoapClient
     */
    private $soapClient;

    /**
     * @var BaseObjectFactory
     */
    private $objectFactory;

    /**
     * @var InvokeService
     */
    private $invokeService;

    /**
     * @param string $interface
     * @param \SoapClient $soapClient
     * @param BaseObjectFactory $objectFactory
     */
    public function __construct(string $interface, \SoapClient $soapClient, BaseObjectFactory $objectFactory) {
        $this->interface = $interface;
        $this->soapClient = $soapClient;
        $this->objectFactory = $objectFactory;
    }

    /**
     * @param string $name Method name
     * @param array $arguments parameter's method
     * @return mixed
     * @throws InvokeServiceException
     * @throws MethodNotFoundException
     */
    public function __call($name, $arguments) {
        try {
            $this->invokeService = new InvokeService($this->soapClient, $this->objectFactory, $this->interface, $name);
            $this->invokeService->__init();
            $this->invokeService->__validateMethod();
            $this->invokeService->__validateMethodFactory();
            $this->invokeService->__createAbstractRequest();
            $this->invokeService->__validateMethodPrepare();
            $this->invokeService->__prepare($arguments);
            $this->invokeService->__soapParam();
            $response = $this->invokeService->__invokeService();
            $processResponse = new ProcessResponse();
            $processResponse->setResponse($response);
            return $processResponse->__process();
        } catch (MethodNotFoundException $ex) {
            throw new MethodNotFoundException($ex->getMessage());
        } catch (InvokeServiceException $ex) {
            throw new InvokeServiceException($ex);
        }
    }

    /**
     * @return mixed
     */
    public function __getLastRequest() {
        return $this->invokeService->__getLastRequest();
    }

    /**
     * @return mixed
     */
    public function __getLastResponse() {
        return $this->invokeService->__getLastResponse();
    }

}
