<?php

namespace Croydon\Servicios;

/**
 * Description of AbstractRequest
 *
 * @author jonathan
 * @method void __prepare($args) Must be implemented
 */
abstract class AbstractRequest implements InterfaceRequest {

    /**
     * @return array
     */
    public function toRequest(): array {
        $reflection = new \ReflectionClass($this);
        $properties = $reflection->getDefaultProperties();
        $toRequest = array();
        foreach ($properties as $property => $value) {
            $getRecursiveRequest = new GetRecursiveRequest($this);
            $toRequest[$property] = $getRecursiveRequest->__getValue($this, $property);
        }
        return $toRequest;
    }

    /**
     * @return array
     */
    protected function __getValues(string $method, array $values): array {
        $reflection = new \ReflectionClass($this);
        $parameters = $reflection->getMethod($method)->getParameters();
        $data = array();
        foreach ($parameters as $key => $parameter) {
            $data[$parameter->name] = $values[$key];
        }
        return $data;
    }

}
