<?php

namespace Croydon\Servicios;

/**
 * Description of GetRecursiveRequest
 *
 * @author jonathan
 */
class GetRecursiveRequest {

    /**
     * @var object
     */
    private $request;

    /**
     * @param object $request
     * @param \ReflectionClass $reflection
     */
    public function __construct($request) {
        $this->request = $request;
    }
    
    /**
     * @param object $request
     * @param string $property
     * @return \SoapVar|array()
     */
    public function __getValue($request, string $property) {
        $method = new \ReflectionMethod($request, sprintf('get%s', ucfirst($property)));
        $invoke = $method->invoke($request);
        if (is_object($invoke) || is_array($invoke)) {
            $properties = $invoke;
            $toRequest = array();
            if (is_object($invoke)) {
                $reflection = new \ReflectionClass($invoke);
                $properties = $reflection->getDefaultProperties();
                $toRequest = $this->__getObjectValue($invoke, $properties);
            } else {
                $invoke = $request;
                $toRequest = $this->__getArrayValue($request, $properties);
            }
            return new \SoapVar($toRequest, SOAP_ENC_OBJECT, null, 'ns1', $property);
        }
        return new \SoapVar($invoke, XSD_STRING, null, 'xsd', $property, '');
    }

    /**
     * @param object $request
     * @param array $properties
     * @return array()
     */
    private function __getObjectValue($request, array $properties) {
        $toRequest = array();
        foreach ($properties as $property => $value) {
            $toRequest[$property] = $this->__getValue($request, $property);
        }
        return $toRequest;
    }

    /**
     * @param object $request
     * @param array $properties
     * @return array()
     */
    private function __getArrayValue($request, array $properties) {
        $toRequest = array();
        foreach ($properties as $property => $value) {
            if (is_string($property)) {
                $toRequest[$property] = $value;
            } else {
                $toRequest[$property] = $value;
            }
        }
        return $toRequest;
    }

    /**
     * 
     */
    private function __getValueSubObject($object) {
        
    }

}
