<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Croydon\Servicios;

/**
 * Description of InvokeServiceException
 *
 * @author jonathan
 */
class InvokeServiceException extends \Exception {

    public function __construct(\Exception $ex) {
        $code = !is_null($ex->getCode()) && !empty($ex->getCode()) ? $ex->getCode() : 0;
        parent::__construct($ex->getMessage(), $code, $ex);
    }

}
