<?php

namespace Croydon\Servicios;

use Croydon\Servicios\Aurora\ErrorResponse;
use Croydon\Servicios\Aurora\FacConsultaEstadoCarteraResponse;
use Croydon\Servicios\Aurora\InvConsultaDispResponse;
use Croydon\Servicios\Pse\CreateTransactionPaymentResponse;
use Croydon\Servicios\Pse\CreateTransactionResponseType;
use Croydon\Servicios\Pse\GetTransactionInformationResponse;
use Croydon\Servicios\Pse\GetTransactionInformationResponseType;

/**
 * Description of ProcessResponse
 *
 * @author jonathan
 */
class ProcessResponse {

    /**
     * Content Response
     * @var mixed
     */
    private $response;

    /**
     * @var string
     */
    private $classToUse;

    /**
     * @var string
     */
    private $errorResponse;

    /**
     * @return string
     */
    public function getResponse() {
        return $this->response;
    }

    /**
     * @return string
     */
    public function getClassToUse() {
        return $this->classToUse;
    }

    /**
     * @return string
     */
    public function getErrorResponse() {
        return $this->errorResponse;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response) {
        $this->response = $response;
    }

    /**
     * @param string $classToUse
     */
    public function setClassToUse(string $classToUse) {
        $this->classToUse = $classToUse;
    }

    /**
     * @param string $errorResponse
     */
    public function setErrorResponse(string $errorResponse) {
        $this->errorResponse = $errorResponse;
    }

    /**
     * Procesa la respuesta y la convierte en terminos de objetos, si no esta parametrizado devuelve la respuesta original
     */
    public function __process() {
        $return = $this->response;
        if (isset($this->response->return)) {
            $return = json_decode($this->response->return);
            if (isset($return->EXEPCION)) {
                $errorResponse = new ErrorResponse();
                $errorResponse->setCodigo($return->EXEPCION->CODIGO);
                $errorResponse->setDescripcion($return->EXEPCION->DESCRIPCION);
                return $errorResponse;
            }
        }
        if (isset($return->CantDisponible)) {
            $invConsultaDispItemBodResponse = new InvConsultaDispResponse();
            $invConsultaDispItemBodResponse->setCantDisponible($return->CantDisponible);
            return $invConsultaDispItemBodResponse;
        }
        if (isset($return->cupodisponible)) {
            $facConsultaEstadoCarteraResponse = new FacConsultaEstadoCarteraResponse();
            $facConsultaEstadoCarteraResponse->setCupodisponible((float) $return->cupodisponible);
            $facConsultaEstadoCarteraResponse->setFactura($return->factura);
            $facConsultaEstadoCarteraResponse->setPorredimir((float) $return->porredimir);
            $facConsultaEstadoCarteraResponse->setSaldo((float) $return->saldo);
            $facConsultaEstadoCarteraResponse->setVlrnotacre($return->vlrnotacre);
            return $facConsultaEstadoCarteraResponse;
        }
        if (isset($return->coddoc)) {
            $facActOrdenPedidoResponse = new Facturacion\FacActOrdenPedidoResponse();
            $facActOrdenPedidoResponse->setCoddoc($return->coddoc);
            $facActOrdenPedidoResponse->setMsg($return->msg);
            $facActOrdenPedidoResponse->setNumdoc($return->numdoc);
            $facActOrdenPedidoResponse->setSucdoc($return->Sucdoc);
            return $facActOrdenPedidoResponse;
        }
        if (isset($return->getTransactionInformationResult)) {
            $object = $return->getTransactionInformationResult;
            $getTransactionInformationResposeType = new GetTransactionInformationResponseType();
            $getTransactionInformationResposeType->setBankName($object->BankName);
            $getTransactionInformationResposeType->setBankProcessDate($object->BankProcessDate);
            $getTransactionInformationResposeType->setEntityCode($object->EntityCode);
            $getTransactionInformationResposeType->setPayCurrency($object->PayCurrency);
            $getTransactionInformationResposeType->setPaymentSystem($object->PaymentSystem);
            if (isset($object->ReferenceArray)) {
                $getTransactionInformationResposeType->setReferenceArray($object->ReferenceArray);
            }
            $getTransactionInformationResposeType->setRetriesTicketId($object->TicketId);
            $getTransactionInformationResposeType->setReturnCode($object->ReturnCode);
            $getTransactionInformationResposeType->setCurrencyRate($object->CurrencyRate);
            $getTransactionInformationResposeType->setTicketId($object->TicketId);
            $getTransactionInformationResposeType->setTranState($object->TranState);
            $getTransactionInformationResposeType->setTransVatValue($object->TransVatValue);
            $getTransactionInformationResposeType->setTrazabilityCode($object->TrazabilityCode);
            $getTransactionInformationResposeType->setTransValue($object->TransValue);
            $getTransactionInformationRespose = new GetTransactionInformationResponse();
            $getTransactionInformationRespose->setGetTransactionInformationResult($getTransactionInformationResposeType);
            return $getTransactionInformationRespose;
        }
        if (isset($return->createTransactionPaymentResult)) {
            $object = $return->createTransactionPaymentResult;
            $createTransactionPaymentResult = new CreateTransactionResponseType();
            $createTransactionPaymentResult->setECollectUrl($object->eCollectUrl);
            $createTransactionPaymentResult->setReturnCode($object->ReturnCode);
            $createTransactionPaymentResult->setTicketId($object->TicketId);
            $createTransactionPaymentResponse = new CreateTransactionPaymentResponse();
            $createTransactionPaymentResponse->setCreateTransactionPaymentResult($createTransactionPaymentResult);
            return $createTransactionPaymentResponse;
        }
        return $this->response;
    }

}
