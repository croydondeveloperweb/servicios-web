<?php

namespace Croydon\Servicios;

/**
 *
 * @author jonathan
 */
interface InterfaceRequest {

    /**
     * 
     * @return array
    */
    public function toRequest(): array;
}
