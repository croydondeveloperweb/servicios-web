<?php

require './vendor/autoload.php';

//--------------------------
//AURORA SERVICE
//--------------------------

try {
    $auroraService = new \Croydon\Servicios\Aurora\AuroraService('http://192.168.0.232:8080/Gauss/FacturacionService?WSDL');
//$auroraService->getFacturacionPort()->facConsultaEstadoCartera($uid, $cia, $ano, $app, $cliente)->getSaldo();
//    $InvConsultaDispResponse = $auroraService->getFacturacionPort()->invConsultaDispItemBod('8001206812', '01', date('Y'), '03', 'ZI58720-SS', 100);
//    var_dump($InvConsultaDispResponse);
//    var_dump($InvConsultaDispResponse->getCantDisponible());
//    var_dump($auroraService->getFacturacionPort()->__getLastRequest());
//    $auroraService->getFacturacionPort()->InvConsultaItemTiendas();
//    $auroraService->getFacturacionPort()->carAplicacionFacturas($uid, $cia, $ano, $app, $cliente, $sucursal, $factura, $parcial);
//    $auroraService->getFacturacionPort()->facConsultaEstadoCartera($uid, $cia, $ano, $app, $cliente);
//    $auroraService->getFacturacionPort()->facConsultaGuia($uid, $cia, $ano, $app, $orden);
//    $auroraService->getFacturacionPort()->invConsultaDispCroydonista($uid, $cia, $ano, $app, $codItem, $cantidad);
//    $auroraService->getFacturacionPort()->invConsultaDispItemBod($uid, $cia, $ano, $app, $codItem, $cantidad);
//    $auroraService->getFacturacionPort()->invConsultaItemIncentivo($uid, $cia, $ano, $app, $codItem, $cantidad);
//    var_dump($auroraService->getFacturacionPort()->invConsultaDispCroydonista('8001206812', '01', '2019', '03', 'ZN34699-46', 10000));
//    var_dump($auroraService->getFacturacionPort()->invConsultaDispCroydonista('8001206812', '01', '2019', '03', 'ZI58720-SS', 10000));
    var_dump($auroraService->getFacturacionPort()->facConsultaEstadoCartera('8001206812', '01', 2019, 03, '1094168560'));
//    var_dump($auroraService->getFacturacionPort()->facConsultaGuia('8001206812', '01', 2019, '03', '100072514'));
//    var_dump($auroraService->getFacturacionPort()->invConsultaItemIncentivo('8001206812', '01', 2019, '03', 'AD34090-33', 1));
} catch (Croydon\Servicios\InvokeServiceException $ex) {
    var_dump($ex);
    var_dump($ex->getMessage());
} catch (\Croydon\Servicios\MethodNotFoundException $e) {
    var_dump($e);
}

//--------------------------
//FACTURACION SERVICE
//--------------------------
try {
    $facturacionService = new Croydon\Servicios\Facturacion\FacturacionService();
//    $facturacionService->getFacturacionPort()->facActFactura($nit, $compania, $ano, $modulo, $cedula, $tipo_documento, $nombres_factura, $apellidos_factura, $email, $direccion_factura, $direccion_despacho, $nombres_despacho, $apellidos_despacho, $municipio_factura, $municipio_despacho, $pais, $telefono_factura, $celular_factura, $telefono_despacho, $celular_despacho, $codigo_clase, $codigo_condicion_pago, $codigo_descuento, $fecha_documento, $orden_compra, $subtotal_bruto, $subtotal_fletes, $total_cargos, $iva, $total_descuentos, $total_neto, $numDoc, $detalle);
//    $facturacionService->getFacturacionPort()->facActOrdenPedido($nit, $compania, $ano, $modulo, $cedula, $tipo_documento, $nombres_factura, $apellidos_factura, $email, $direccion_factura, $direccion_despacho, $nombres_despacho, $apellidos_despacho, $municipio_factura, $municipio_despacho, $pais, $telefono_factura, $celular_factura, $telefono_despacho, $celular_despacho, $codigo_clase, $codigo_condicion_pago, $codigo_descuento, $fecha_documento, $orden_compra, $subtotal_bruto, $subtotal_fletes, $total_cargos, $iva, $total_descuentos, $total_neto, $detalle);
//    $facturacionService->getFacturacionPort()->facActRedencionIncentivos($nit, $compania, $ano, $modulo, $cedula, $items);
    $detalle = array(
        array("ZH97890-MM", "19990", "1", "A"),
        array("ZI31010-SS", "19990", "1", "A"),
        array("ZH90342-MM", "38990", "1", "A"),
        array("ZH81010-MM", "26990", "1", "A"),
        array("AO52060-39", "84900", "1", "A"),
        array("AI91045-24", "29900", "1", "A"),
        array("ZE30950-UN", "15990", "1", "A"),
        array("ZE18610-UN", "9990", "1", "A"),
        array("AF73099-36", "29900", "1", "A"),
        array("ZE43210-UN", "12990", "1", "A")
    );
    $facActOrdenPedidoResponse = $facturacionService->getFacturacionPort()->facActOrdenPedido('8001206812', '01', date('Y'), '03', '1073156557', 'CC', 'SANDRA MILENA', 'MORA GIL', 'morasandra29@hotmail.com', 'CL 6 3 20 SAN FRANCISCO CUNDINAMARCA MADRID', 'CL 6 3 20 SAN FRANCISCO CUNDINAMARCA MADRID', 'SANDRA MILENA', 'MORA GIL', '25430000', '25430000', '0169', '5728098310', '3194325146', '5728098310', '3194325146', 'VC', '03', '25', '20180715', "6654646", '243387', '243387', '243387', '34683', '60847', '217222', $detalle);
    var_dump($facActOrdenPedidoResponse);
    var_dump($facturacionService->getFacturacionPort()->facActFactura('8001206812', '01', date('Y'), '03', '1073156557', 'CC', 'SANDRA MILENA', 'MORA GIL', 'morasandra29@hotmail.com', 'CL 6 3 20 SAN FRANCISCO CUNDINAMARCA MADRID', 'CL 6 3 20 SAN FRANCISCO CUNDINAMARCA MADRID', 'SANDRA MILENA', 'MORA GIL', '25430000', '25430000', '0169', '5728098310', '3194325146', '5728098310', '3194325146', 'VC', '03', '25', '20180715', "6654646", '243387', '243387', '243387', '34683', '60847', '217222', $facActOrdenPedidoResponse->getNumdoc(), $detalle));
} catch (Exception $ex) {
//    var_dump($ex);
}


